window.App = {
	loadViewId : null,
	currentViewId : null,
	previousViewId : null,
	contributorId : null,
	userId : null,
	user : null,
	requestCounter : 0,
	currentPosition : null,
	transitionType : "flip",
	menu : null,
	view : {},
	geocoder : null,
	inBackground:false,
	contributors:null,
	history:[],
	historyIndex:-1,
	userApprovedLocationServices:false,
	currentPositionSource : {
		coords : {
			latitude : 39.7391667,
			longitude : -104.9841667
		}
	},
	init : function() {
		var app = window.App;
		
		var pageDetails = $.parseJSON(window.localStorage.getItem('pageDetails'));
		if (pageDetails) {
			app.loadViewId = $.getOrDefault(pageDetails.viewId, 'profile');
			app.loadData = $.getOrDefault(pageDetails.data, {});
			app.currentViewId = $.getOrDefault(pageDetails.currentViewId, 'profile');
		}
		
		$(document).on("pagechange", function(event) {

			var title = "CAN I KICK IT";
			document.title = title;

			$('[data-role="header"],.menu-button').show();

			$('.menu-selected-item').removeClass('menu-selected-item');
			$('a[data-key="' + app.currentViewId + '"]').addClass('menu-selected-item');

			app.view[app.currentViewId].behavior();

			var copywrite = $.systetmVarByKey('copywrite');
			if (copywrite) {
				$('.copywrite').html(copywrite['SystemVariable']['value']);
			}
			setBodyHeight();
		}).bind('swiperight swipeleft', function() {
			if (app.currentViewId != 'signin' && app.currentViewId != 'login' && app.currentViewId != 'register' && app.currentViewId != 'passwordreset') {
				$.getPosition(function(currentPosition){
					$.ajaxProxy({
						url : "contributors.json",
						type : 'GET',
						data : {},
						callback : function(response) {
							var notFound = true;
							$.each(response.rows, function() {
								if (notFound) {
									var id = this.id;
									var address = this.address + ' ' + this.city + ' ' + this.state + ' ' + this.zip;
									var geocoder = new google.maps.Geocoder();
									window.App.geocoder.geocode({
										address : address
									}, function(locResult) {
										var lat = locResult[0].geometry.location.lat();
										var lng = locResult[0].geometry.location.lng();
										var latLng = new google.maps.LatLng(lat, lng);
										var myLatLn = new google.maps.LatLng(
											currentPosition.coords.latitude, 
											currentPosition.coords.longitude
										);
										if ($.isInRange(latLng, myLatLn)) {
											notFound = false;
											window.App.loadView('contributor', id);
										}
									});
								}
							});
	
							if (notFound) {
								app.loadView('overview');
							}
						}
					}, true);
				});
			}
		});
		
		if(typeof(cordova) != 'undefined' && cordova.plugins) {
				
			cordova.plugins.backgroundMode.enable({ text:'Monitoring location to notify you if any CIKI locations are near you.'});
			
			cordova.plugins.backgroundMode.onactivate = function () {
		        app.inBackground = true;
		    };
		    
		    cordova.plugins.backgroundMode.ondeactivate = function () {
		        app.inBackground = false;
		    };
		    
		    cordova.plugins.notification.local.on("click", function (notification) {
		    	window.App.loadView('overview');
			});
		}

		function setBodyHeight() {
			$('.ui-content').css({
				height : $(window).height() - $('[data-role="header"]').height()
			});
			$('.overlay').css('height', $(window).height() - 75);
		}

		setBodyHeight();

		app.autoLogin();
	},
	notifyContributorInRange : function() {
		var app = window.App;
		if (app.currentViewId != 'signin' && app.currentViewId != 'login' && app.currentViewId != 'register' && app.currentViewId != 'passwordreset' && window.App.userApprovedLocationServices) {
			function checkNotify(location){
				$.loadContributors(function(data){
					var contributors = data.rows;
					var inRangeContributors = [];
					if (contributors.length > 0) {
						var processedConributors = 0;
						$.each(contributors, function(i, contributor) {
							var item = contributor;
							var title = item.username.toUpperCase();

							var address = item.address + ' ' + item.city + ' ' + item.state + ' ' + item.zip;

							window.App.geocoder.geocode({
								address : address
							}, function(locResult) {
								
								if (locResult != null) {
									var lat = locResult[0].geometry.location.lat();
									var lng = locResult[0].geometry.location.lng();
									var latLng = new google.maps.LatLng(lat, lng);

									var mylat = location.latitude;
									var mylng = location.longitude;
									var myLatLn = new google.maps.LatLng(mylat, mylng);
									
									if ($.isInRange(latLng, myLatLn)) {
										inRangeContributors.push(contributor);
									}
								}

								processedConributors++;
								
								if (processedConributors == contributors.length && inRangeContributors.length > 0) {
									inRangeContributors.sort(function(a, b) {
										if (a.username > b.username) {
											return -1;
										} else {
											return 1;
										}
									});
									
									var message = "";
									$.each(inRangeContributors, function(i, contributor) {
										if (i > 0) {
											message += ", ";
										}
										message += contributor.username;
									});
									message += '.';
									var day = $.dayOfYear(new Date());
									var key = message + day;
									var title = window.App.view.global.labels.contributorsInAreaAlertTitle;
									
									if (window.localStorage.getItem(key) == null) {
										
										function plainAlert(title, message){
											var html = '<div class="in_range_contributors" ><h1 style="font-size:20px !important;color:#E5F32D;">' + title + '</h1><br/>';
											$.each(inRangeContributors, function(){
												html += "<a href='#' data-id='" + this.id + "' style='font-size:18px;vertical-align:middle;color:white;text-transform:uppercase;text-decoration:none;'  >";
												html += "<img src='" + this.featured_image + "' style='width:30px;vertical-align:middle;' />&nbsp;&nbsp;"; 
												html += this.username; 
												html += "</a>";
											});
											html += "</div>";
											alert(html, title);
											$('.in_range_contributors a').button();
											$('.in_range_contributors a').closest('.ui-btn').click(function(){
												$('.close').trigger('click');
												window.App.loadView('contributor', $(this).find('a').attr('data-id'));
											});
										}
										
										try {
											if (cordova.plugins && window.App.inBackground) {
												function scheduleLocal(){
													cordova.plugins.notification.local.schedule({
													    text:    message,  
													    title:      title
													});
												}
												cordova.plugins.notification.local.hasPermission(function (granted) {
												    if(granted){
												    	scheduleLocal();
												    }else{
												    	cordova.plugins.notification.local.registerPermission(function (granted) {
														    if(granted){
														    	scheduleLocal();
														    }
														});
												    }
												});
											} else {
												plainAlert(title, message);
											}
										} catch(e) {
											plainAlert(title, message);
										}
										
										window.localStorage.setItem(key, "true");
									  }
								}
							});
						});
					}
				});
			}
			
			$.getPosition(checkNotify);
		}
	},
	showTips : function() {
		$.ajaxProxy({
			url : "tips.json",
			type : "get",
			callback : function(d) {
				if (d.rows.length == 0) {
					var html = 'There are currently no tips available.';
				} else {
					var html = '<div class="tips-slider" data-fx="scroll" data-timeout="5000">';
					$.each(d.rows, function(i, item) {
						var p = item;
						html += '<img src="' + p.featured_image + '"  />';
					});
					html += '</div>';
				}
				alert(html);
				//$('.tips-slider').tcycle();
			}
		});
	},
	loadPreviousView : function() {
		window.App.historyIndex = window.App.historyIndex - 1;
		console.log(window.App.historyIndex);
		var history = window.App.history[window.App.historyIndex];
		window.App.loadView($.getOrDefault(history.id, 'profile'), $.getOrDefault(history.data, {}));
		window.App.historyIndex = window.App.historyIndex - 1;
		window.App.history.pop();
	},
	loadView : function(viewId, data) {
		Pace.track(function() {
			if ( typeof data != 'object') {
				data = {
					id : data
				};
			}
			window.App.historyIndex = window.App.historyIndex + 1;
			window.App.history.push({
				id:window.App.currentViewId,
				data: window.App.currentViewData
			});
			
		//	window.App.previousViewData = window.App.currentViewData;
		//	window.App.previousViewId = window.App.currentViewId;

			window.App.currentViewId = viewId;
			window.App.currentViewData = data;

			$('#' + window.App.previousViewId).remove();

			$.mobile.changePage(viewId + "_view.html", {
				transition : window.App.transitionType,
				data : data,
				showLoadMsg : false,
				pageContainer : $(".page-content-container")
			});

			window.localStorage.setItem('pageDetails', JSON.stringify({
				viewId : viewId,
				data : data
			}));
		});
	},
	buildConfiguration : function() {
		function sVal(key) {
			return $.systetmVarByKey(key)['SystemVariable']['value'];
		}
		
		openFB.init({
			appId : $.systetmVarByKey('facebook_api_id')['SystemVariable']['value']
		});
		
		IG = new IG($.systetmVarByKey('instagram_client_id')['SystemVariable']['value'],
		$.systetmVarByKey('instagram_client_secret')['SystemVariable']['value']
		);
		
		GOOGLE = new GOOGLE($.systetmVarByKey('google_api_id')['SystemVariable']['value'],
			$.systetmVarByKey('google_client_secret')['SystemVariable']['value']
		);
		
		TWITTER = new TWITTER($.systetmVarByKey('twitter_consumer_key')['SystemVariable']['value'], 
			$.systetmVarByKey('twitter_consumer_secert')['SystemVariable']['value']);

		window.App.view = $.parseJSON(sVal('app_labels'));

		$('.menu-options').menuOptions();

		window.App.menu = $.jPanelMenu({
			menu : '.menu-options',
			trigger : '.menu-button',
			animated : true,
			openPosition : '90%'
		});

		window.App.menu.on();

		$('#jPanelMenu-menu a').click(function(e) {
			e.preventDefault();
			e.stopPropagation();

			var href = $(this).attr('href');
			$('.menu-button').trigger('click');

			window.location = href;
		});
		
		
		$('[data-role="header"] .map-button').click($.mapOverlay);

		var scripts = ["js/behavior/app.incorrect_version.behavior.js", "js/behavior/app.signin.behavior.js", "js/behavior/app.trips.behavior.js", "js/behavior/app.socialmedia.behavior.js", "js/behavior/app.about.behavior.js", "js/behavior/app.overview.behavior.js", "js/behavior/app.profile.behavior.js", "js/behavior/app.contributor.behavior.js", "js/behavior/app.estlibishment.behavior.js", "js/behavior/app.passwordreset.behavior.js", "js/behavior/app.login.behavior.js", "js/behavior/app.faq.behavior.js",  "js/behavior/app.register.behavior.js", "js/behavior/app.settings.behavior.js", "https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false&callback=initialize&key=" + sVal('google_maps_id') + "&sensor=true&libraries=geometry"];

		$.each(scripts, function(i, script) {
			$.loadScript(script);
		});
		
		window.initialize = function() {
			window.App.geocoder = new google.maps.Geocoder();
			$.loadScript("js/lib/CustomGoogleMapMarker.js");
			$('.map-button').show();
		};
	},
	autoLogin : function() {
		var app = window.App;
		$.ajaxProxy({
			url : "users/init",
			callback : function(response) {
				app.systemVariables = response.systemVariables;
				app.buildConfiguration();
				if($.systetmVarByKey('app_version')['SystemVariable']['value'].indexOf(APP_VERSION) == -1){
					window.App.loadView('incorrect_version');
				}else{
					if (response.user != null) {
					$.afterLogin(response);
					} else {
						var loginDetails = $.loadLoginDetails();
						if (loginDetails != null) {
							$.login(loginDetails, function(d) {
								$.afterLogin(d);
							});
						} else {
							app.loadView('login');
						}
					}
				}
			}
		});
	}
};

