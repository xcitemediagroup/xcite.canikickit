$(function() {
	window.App.view.incorrect_version.behavior = function() {
		$('.menu-button').hide();
		$('[data-role="header"]').hide();
		$('.close, .overlay').remove();
		$('#incorrect_version [data-role="content"] h2').html(window.App.view.incorrect_version.labels.errorMessage);
		return {};
	};
});
