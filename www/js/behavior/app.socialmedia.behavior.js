$(function() {
	window.App.view.socialmedia.behavior = function() {
		var app = window.App;
		var user = app.user;
		var hashtag = window.App.view.socialmedia.labels.defaultSharingHashtag;
		var instructions = window.App.view.socialmedia.labels.sharingInstructions.replace('[sharing-hashtag]', hashtag);
		user.instructions = instructions;
		$('#challanges [data-role="content"]').html($.template('social_sharing', user));
		function applyUser(u) {
			$(".social_sharing .profile-image").css('background-image', 'none');
			$(".social_sharing .profile-image").css('background-image', 'url(' + BASE_FULL_URL + "featured_image/get/User/" + u.id + '?r=' + $.randomString() + ')');
			$('.username').html($.truncate(u.username.toUpperCase(), 6));

			for (var i in u) {
				var $f = $('[name="' + i + '"]');
				if ($f.attr('type') != 'file') {
					$('[name="' + i + '"]').val(u[i]);
				}
			}
		}
		$(".social_sharing .profile-image").click(function(){
			$.avatarChooser(function(bytes){
				$('#featured_image').val(bytes);
				$('#profile-form').ajaxFormSubmit(function(data) {
					var obj = {};
					$.each(data, function(i, item) {
						obj[item.name] = item.value;
					});
					obj.active = 1;
					$.ajaxProxy({
						url : "/users/" + obj['id'] + ".json",
						type : 'post',
						data : obj,
						callback : function(re) {
							for (var i in obj) {
								user[i] = obj[i];
							}
							$.saveLoginDetails(user);
							window.App.loadView('socialmedia');
						}
					});
				});
			});
		});
		applyUser(user);
  
		$('.share-table img').click(function(){
			$[$(this).attr('data-share')](window.App.view.socialmedia.labels.defaultSharingMessage);
		});
		
		$('.share-image').click(function(){
			$.notify('Please use the icons above for sharing.');
		});
		
		$('.slider').bannerSlider();

		return {};
	};
});



