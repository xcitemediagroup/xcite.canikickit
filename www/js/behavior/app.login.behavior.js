$(function() {
	window.App.view.login.behavior = function() {
		var app = window.App;
		$('.menu-button').hide();
		$('[data-role="header"]').hide();
		$('.close, .overlay').remove();
		$(".kickit-sign-in-btn").click(window.App.view.login.kickitSignInBehavior);
		$(".kickit-login-btn").click(window.App.view.login.kickitBehavior);
		$("[data-social-type='facebook']").click(window.App.view.login.facebookBehavior);
		$("[data-social-type='twitter']").click(window.App.view.login.twitterBehavior);
		$("#google-signin-button").click(window.App.view.login.googleBehavior);
		return {};
	};
	
	window.App.view.login.facebookBehavior = function(response) {
		$.facebookLogin(function(connected){
			if(connected){
				Pace.track(function() {
					openFB.api({
		            	path:'/me',
		            	success: function(response) {
		            		console.log(response);
							$.login({
								socialType : 'facebook',
								username : response.name,
								featured_image:"http://graph.facebook.com/" + response.id + "/picture"
							}, function(s) {
								$.afterLogin(s);
							});
						}
		            });
	         	});
           }
		});
	};
	
	window.App.view.login.twitterBehavior = function(response) {
		$.twitterLogin(function(connected, obj){
			if(connected){
				Pace.track(function() {
					TWITTER.bird.__call(
					    "users_show",
					    {screen_name: obj.screen_name},
					    function (reply) {
					        $.login({
								socialType : 'twitter',
								username : obj.screen_name,
								featured_image:reply.profile_image_url
							}, function(s) {
								$.afterLogin(s);
							});
					    }
					);
				});
			}
		});
	};

	window.App.view.login.googleBehavior = function(authResult) {
		$.googleLogin(function(success, obj){
			if(success){
				$.login({
					socialType : 'google',
					username : obj.name.givenName + " " + obj.name.familyName,
					featured_image : obj.image.url
				}, function(s) {
					$.afterLogin(s);
				});
			}
		}, false);
	};
	
	window.App.view.login.kickitSignInBehavior = function() {
		window.App.loadView('signin');
	};

	window.App.view.login.kickitBehavior = function() {
		window.App.loadView('register');
	};
});
