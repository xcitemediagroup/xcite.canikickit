function TWITTER(clientId, clientSecert) {
	var tokenName = "TWITTER_TOKEN";
	var tokenSecertName = "TWITTER_TOKEN_SECERT";
	var loginWindow = null;
	var runningInCordova = true;
	var bird = new Codebird;
	var processed = false;
	var returnURL = "http://canikickitapp.com/system/twitter.oauth.html";
	bird.setConsumerKey(clientId, clientSecert);

	var response = {
		bird: bird,
		post : function(message, hashtag, callback) {
			Pace.track(function() {
				bird.__call("statuses_update", {
					"status" : message + " " + hashtag
				}, function(reply) {
					console.log(reply);
					callback(reply);
				});
			});
		},
		login : function(callback) {
			Pace.track(function() {
				bird.__call("oauth_requestToken", {
					oauth_callback : returnURL
				}, function(reply) {
					bird.setToken(reply.oauth_token, reply.oauth_token_secret);
					bird.__call("oauth_authorize", {}, function(auth_url) {
						loginWindow = window.open(auth_url, 'twitter', 'location=no');
						window.codebird_auth = loginWindow;
						
						function loadStop(event) {
							var url = event.url;
							
							if (url.indexOf(returnURL) > -1) {
								var verifier = $.getUrlVarsByString(event.url)['oauth_verifier'];
								Pace.track(function() {
									bird.__call("oauth_accessToken", {
										oauth_verifier : verifier
									}, function(reply) {
										bird.setToken(reply.oauth_token, reply.oauth_token_secret);
										
										window[tokenName] = reply.oauth_token;
										window[tokenSecertName] = reply.oauth_token_secret;
										processed = true;
										loginWindow.close();
										callback(true, reply);
									});
								});
							}
						}
	
						function loadExit() {
							loginWindow.removeEventListener('loadstop', loadStart);
							loginWindow.removeEventListener('exit', loadExit);
							loginWindow = null;
							if (!processed) {
								_callback(false);
							}
						}
	
	
						loginWindow.addEventListener('loadstop', loadStop);
						loginWindow.addEventListener('exit', loadExit);
					});
				});
			});
		},
		logout : function(callback) {
			window[tokenName] = null;
			window[tokenSecertName] = null;
			callback(true);
		},
		getLoginStatus : function(callback) {
			var token = window[tokenName];
			var secert = window[tokenSecertName];		
			if(token != null && secert != null){
				bird.setToken(token, secert);
			}	
			callback(token != null && secert != null);
		}
	};
	
	return response;
}
