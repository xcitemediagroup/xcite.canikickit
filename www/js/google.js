function GOOGLE(clientId, clientSecert) {
	var returnURL = "http://canikickitapp.com/system/google.oauth.html";
	var url = "https://accounts.google.com/o/oauth2/auth?" + $.param({
		client_id : clientId,
		redirect_uri : returnURL,
		response_type : 'code',
		approval_prompt:'auto',
		access_type : "online",
		scope : "profile"
	});
	var tokenName = "GOOGLE_TOKEN";
	var loginWindow = null;
	var processed = false;

	function _callback(val, data, callback) {
		if (callback != null) {
			callback(val, data);
			processed = true;
		}
	}

	return {
		post : function(message, callback) {
			Pace.track(function() {
				var options = {};
				options.data = {
					"access_token" : window[tokenName],
					"kind" : "plus#moment",
					"type" : "http://schema.org/AddAction",
					"object" : {
						"kind" : "plus#itemScope",
						"type" : "http://schema.org/AddAction",
						"description" : message
					}
				};
				options.url = "https://www.googleapis.com/plus/v1/people/me/moments/vault";
				$.proxyPost(options, function(data) {
					callback(true);
				});
			});
		},
		login : function(callback) {
			processed = false;
			loginWindow = window.open(url, '_blank', 'location=no');

			loginWindow.addEventListener("loadstop", loadStop);
			
			function loadStop(event) {
				
				if (event.url.indexOf(returnURL) > -1){
					var code = $.getUrlVarsByString(event.url)['code'];
					if (code != null) {
						var options = {};
						options.data = {
							code : code,
							client_id : clientId,
							client_secret : clientSecert,
							redirect_uri : returnURL,
							grant_type : 'authorization_code'
						};
						
						options.url = "https://accounts.google.com/o/oauth2/token";
						
						
						Pace.track(function() {
							$.proxyPost(options, function(data) {
								data = $.parseJSON(data);
								window[tokenName] = data.access_token;
								$.getJSON("https://www.googleapis.com/plus/v1/people/me?access_token=" + data.access_token, function(obj) {
									_callback(true, obj, callback);
								}).fail(function(err) {
									_callback(false, null, callback);
								});
							});
						});
						processed = true;
						loginWindow.close();
					}
				}
			}

			function loadExit() {
				loginWindow.removeEventListener('loadstop', loadStop);
				loginWindow.removeEventListener('exit', loadExit);
				loginWindow = null;
				if (!processed) {
					_callback(false);
				}

			}

			loginWindow.addEventListener('exit', loadExit);
		},
		logout : function(callback) {
			window[tokenName] = null;
			callback(true);
		},
		getLoginStatus : function(callback) {
			callback(window[tokenName] != null);
		}
	};
}
