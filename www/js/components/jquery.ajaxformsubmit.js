(function($) {
	$.fn.extend({
		ajaxFormSubmit : function(submitFunction) {
			return this.each(function() {
				var $this = $(this);
				var $inputs = $this.find('input, select, textarea');
				var $fileInput = null;
				$inputs.each(function() {
					if ($(this).attr('type') == 'file') {
						$fileInput = $(this);
					}
				});
				function __post(fileBytes) {
					var data = $this.serializeArray();
					if ($fileInput != null && $fileInput.length > 0 && $fileInput[0].files.length > 0) {
						data.push({
							name : $fileInput.attr('name'),
							value : fileBytes
						});
						data.push({
							name : 'file_name',
							value : $fileInput.val()
						});
					}
					submitFunction(data);
				}

				if ($fileInput != null && $fileInput.length > 0 && $fileInput[0].files && $fileInput[0].files.length > 0) {
					var reader = new FileReader();
					reader.onload = ( function() {
							return function(evt) {
								__post(evt.target.result);
							};
						}());
					reader.readAsDataURL($fileInput[0].files[0]);
				} else {
					__post();
				}
			});
		}
	});
})(jQuery);
