 function alignBanner(){
 	setTimeout(function(){
 		var margin = 0;
	 	var contentHeight = $('[data-role="content"]')[0].scrollHeight - $('.slider').height();
	 	if($(window).height() > contentHeight){
	 		margin = $(window).height() - contentHeight;
	 	}
	 	$('.slider').css('margin-top', margin);
 	}, 500);
 }

(function($) {
	$.fn.extend({
		bannerSlider : function() {
			return this.each(function() {
				var $this = $(this);
				$.ajaxProxy({
					url : "advertisments.json",
					type : "get",
					callback : function(d) {
						var html = '<ul class="banner-ads bxslider">';
						$.each(d.rows, function(i, item) {
							var p = item;
							html += '<li><div style="background-image:url(' + p.featured_image + ');" data-url="' + p.url + '" ></div></li>';
						});
						html += '</ul>';
						$this.html(html);
						$this.find('.banner-ads div').click(function() {
							window.open($(this).attr('data-url'), "", "");
						});
						
						 $('.bxslider').bxSlider({
						 	pager:false,
						 	auto:true
						 });
						
						 $(window).resize(alignBanner);
						 alignBanner();
					}
				});
			});
		}
	});
})(jQuery);
