(function($) {
	$.extend({
		geoLocation : function() {
			function updateServer(location){
				
				if (window.plugins) {
					bgGeo.finish();
				}	
				
				var sendObj = {location:location};
				
				$.ajaxProxy({
					url : "users/updateLocation/" + window.App.user.id,
					type : 'POST',
					data : {location:JSON.stringify(sendObj)}
				});
			}
			
			window.navigator.geolocation.getCurrentPosition(function(postion) {
				window.App.userApprovedLocationServices = true;
				//var postion = $.getSpoofLocation();
				updateServer({
					latitude:postion.coords.latitude,
					longitude:postion.coords.longitude
				});
			}, function(error){
				window.App.userApprovedLocationServices = false;
				/*var postion = $.getSpoofLocation();
				updateServer({
					latitude:postion.coords.latitude,
					longitude:postion.coords.longitude
				});*/
				console.log(error);
				$.notify(window.App.view.global.labels.locationUnableError);
			});
			
			if (window.plugins) {
				var bgGeo = window.plugins.backgroundGeoLocation;
				var callbackFn = function(location) {
					updateServer(location);
				};
				var failureFn = function(error) {
					console.log(error);
				    $.notify(window.App.view.global.labels.locationUnableError);
				};
				bgGeo.configure(callbackFn, failureFn, {
					url : BASE_FULL_URL + 'users/updateLocation/' + window.App.user.id,
					params : {
						auth_token : $.randomString(),
					},
					headers : {},
					desiredAccuracy : 10,
					stationaryRadius : 20,
					distanceFilter : 30,
					notificationTitle : 'Background tracking', 
					notificationText : 'ENABLED', 
					activityType : 'AutomotiveNavigation',
					debug : false, 
					stopOnTerminate : false 
				});
				bgGeo.start();
				
			}
		}
	});
})(jQuery);