(function($) {
	$.fn.extend({
		contributorBrowser : function(d) {
			return this.each(function(){
				var $this = $(this);
				var html = '<div class="contributor-details-container" >';
				$.each(d.rankingContibutors, function(i, contributor){
					var hasSpecial = contributor.User.PurchasedSpecial.length > 0;
					html += '<div class="contributor-container" data-id="' + contributor.User.id + '"';
					html += ' style="background-color:#000;background-image:url(' + contributor.User.featured_image + ')">';
					html += '<table><tr>';
					var logo = (contributor.User.kick_it_at_location != '1') ? 'kiki-king-logo.png' : 'kiki-visited.png';
					html += '<td style="width:25px;"><div class="number" style="background-image: url(img/' + logo + ');"></div></td>';
					html += '<td><div class="heading">' + contributor.User.username + '<br/>';
					html += '<div>' + contributor.User.address + '&nbsp;&nbsp;|&nbsp;&nbsp;' + formatLocal("US", contributor.User.phone) + '</div></div></td></tr></table>';
					html += '</div>';
				});
				html += '</div>';
				$this.html(html);
				$this.find('.contributor-container').click(function(){
					var id = $(this).attr('data-id');
					window.App.loadView('contributor', id);
				});
			});
		}
	});
})(jQuery);