(function($) {
	$.extend({
		ajaxProxy : function(options, hideLoader) {
			options.timeout = window.RequestTimeout;
			options.cache = true;
			options.crossDomain = true;
			options.url = BASE_FULL_URL + options.url;
			options.type = options.type ? options.type : 'get';
			function runRequest() {
				$.ajax(options).done(function(data) {
					if (options.callback) {
						options.callback(data);
					}
				});
			}
			if(hideLoader){
				runRequest();
			}
			Pace.track(runRequest);

		}
	});
})(jQuery);
