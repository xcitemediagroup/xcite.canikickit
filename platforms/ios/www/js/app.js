window.App = {
	loadViewId : null,
	currentViewId : null,
	previousViewId : null,
	contributorId : null,
	userId : null,
	user : null,
	requestCounter : 0,
	currentPosition : null,
	transitionType : "flip",
	menu : null,
	view : {},
	geocoder : null,
	currentPositionSource : {
		coords : {
			latitude : 39.7391667,
			longitude : -104.9841667
		}
	},
	init : function() {
		var app = window.App;
		var pageDetails = $.parseJSON(window.localStorage.getItem('pageDetails'));
		if (pageDetails) {
			app.loadViewId = $.getOrDefault(pageDetails.viewId, 'profile');
			app.loadData = $.getOrDefault(pageDetails.data, {});
			app.currentViewId = $.getOrDefault(pageDetails.currentViewId, 'profile');
		}
		$(document).on("pagechange", function(event) {
			$.focusAnything('#' + app.currentViewId);

			var title = "CAN I KICK IT";
			document.title = title;

			$('[data-role="header"],.menu-button').show();

			$('.menu-selected-item').removeClass('menu-selected-item');
			$('a[data-key="' + app.currentViewId + '"]').addClass('menu-selected-item');

			app.view[app.currentViewId].behavior();

			var copywrite = $.systetmVarByKey('copywrite');
			if (copywrite) {
				$('.copywrite').html(copywrite['SystemVariable']['value']);
			}
			$('.ui-content').scrollTop(0);
			setBodyHeight();
		}).bind('swiperight swipeleft', function() {
			if (app.currentViewId != 'login' && app.currentViewId != 'register' && app.currentViewId != 'passwordreset') {
				$.ajaxProxy({
					url : "contributors.json",
					type : 'GET',
					data : {},
					callback : function(response) {
						var notFound = true;
						$.each(response.rows, function() {
							if (notFound) {
								var id = this.id;
								var address = this.address + ' ' + this.city + ' ' + this.state + ' ' + this.zip;
								var geocoder = new google.maps.Geocoder();
								window.App.geocoder.geocode({
									address : address
								}, function(locResult) {
									var lat = locResult[0].geometry.location.lat();
									var lng = locResult[0].geometry.location.lng();
									var latLng = new google.maps.LatLng(lat, lng);
									var myLatLn = window.App.currentPosition;
									var SameThreshold = 50;
									if ($.isInRange(latLng, myLatLn)) {
										notFound = false;
										window.App.loadView('contributor', id);
									}
								});
							}
						});

						if (notFound) {
							app.loadView('overview', app.user);
						}
					}
				});
			}

		});

		function setBodyHeight() {
			$('.ui-content').css({
				height : $(window).height() - $('[data-role="header"]').height()
			});
			$('.overlay').css('height', $(window).height() - 75);
		}

		setBodyHeight();

		app.autoLogin();
	},
	notifyContributorInRange : function() {
		if (window.App.currentPosition != null) {
			$.ajaxProxy({
				url : "contributors/activeContributors/",
				type : 'GET',
				data : {},
				callback : function(data) {
					var contributors = data.rows;
					var inRangeContributors = [];
					if (contributors.length > 0) {
						var processedConributors = 0;
						$.each(contributors, function(i, contributor) {
							var item = contributor;
							var title = item.username.toUpperCase();

							var address = item.address + ' ' + item.city + ' ' + item.state + ' ' + item.zip;

							window.App.geocoder.geocode({
								address : address
							}, function(locResult) {
								if (locResult != null) {
									var lat = locResult[0].geometry.location.lat();
									var lng = locResult[0].geometry.location.lng();
									var latLng = new google.maps.LatLng(lat, lng);

									var mylat = window.App.currentPosition.coords.latitude;
									var mylng = window.App.currentPosition.coords.longitude;
									var myLatLn = new google.maps.LatLng(mylat, mylng);

									if ($.isInRange(latLng, myLatLn)) {
										inRangeContributors.push(contributor);
									}
								}

								processedConributors++;
								if (processedConributors == contributors.length && inRangeContributors > 0) {
									inRangeContributors.sort(function(a, b) {
										if (a.username > b.username) {
											return -1;
										} else {
											return 1;
										}
									});
									var message = "There are " + inRangeContributors.length + " CIKI locations near you. They are ";
									$.each(inRangeContributors, function(i, contributor) {
										if (i > 0) {
											message += ", ";
										}
										message += contributor.username;
									});
									message += '.';
									var day = $.dayOfYear(new Date());
									var key = message + day;
									if (window.localStorage.getItem(key) == null) {
										window.localStorage.setItem(key, "true");
										$.notify(message);
									}
								}
							});
						});

					}
				}
			});
		}
	},
	showTips : function() {
		$.ajaxProxy({
			url : "tips.json",
			type : "get",
			callback : function(d) {
				if (d.rows.length == 0) {
					var html = 'There are currently no tips available.';
				} else {
					var html = '<div class="tips-slider" data-fx="scroll" data-timeout="5000">';
					$.each(d.rows, function(i, item) {
						var p = item;
						html += '<img src="' + p.featured_image + '"  />';
					});
					html += '</div>';
				}
				alert(html);
				$('.tips-slider').tcycle();
			}
		});
	},
	loadPreviousView : function() {
		window.App.loadView($.getOrDefault(window.App.previousViewId, 'profile'), $.getOrDefault(window.App.previousViewData, {}));
	},
	loadView : function(viewId, data) {
		Pace.track(function() {
			if ( typeof data != 'object') {
				data = {
					id : data
				};
			}
			window.App.previousViewData = window.App.currentViewData;
			window.App.previousViewId = window.App.currentViewId;

			window.App.currentViewId = viewId;
			window.App.currentViewData = data;

			$('#' + window.App.previousViewId).remove();

			$.mobile.changePage(viewId + "_view.html", {
				transition : window.App.transitionType,
				data : data,
				showLoadMsg : false,
				pageContainer : $(".page-content-container")
			});

			window.localStorage.setItem('pageDetails', JSON.stringify({
				viewId : viewId,
				data : data
			}));
		});
	},
	buildConfiguration : function() {
	
		function sVal(key) {
			return $.systetmVarByKey(key)['SystemVariable']['value'];
		}

		setInterval(window.App.notifyContributorInRange, 60000);

		openFB.init({
			appId : $.systetmVarByKey('facebook_api_id')['SystemVariable']['value']
		});
		
		IG = new IG($.systetmVarByKey('instagram_client_id')['SystemVariable']['value']);
		
		GOOGLE = new GOOGLE($.systetmVarByKey('google_api_id')['SystemVariable']['value'],
			$.systetmVarByKey('google_client_secret')['SystemVariable']['value']
		);
		
		TWITTER = new TWITTER($.systetmVarByKey('twitter_consumer_key')['SystemVariable']['value'], 
			$.systetmVarByKey('twitter_consumer_secert')['SystemVariable']['value']);

		window.App.view = $.parseJSON(sVal('app_labels'));

		$('.menu-options').menuOptions();

		window.App.menu = $.jPanelMenu({
			menu : '.menu-options',
			trigger : '.menu-button',
			animated : true,
			openPosition : '90%'
		});

		window.App.menu.on();

		$('#jPanelMenu-menu a').click(function(e) {
			e.preventDefault();
			e.stopPropagation();

			var href = $(this).attr('href');
			$('.menu-button').trigger('click');

			window.location = href;
		});

		var scripts = ["js/behavior/app.trips.behavior.js", "js/behavior/app.socialmedia.behavior.js", "js/behavior/app.about.behavior.js", "js/behavior/app.overview.behavior.js", "js/behavior/app.profile.behavior.js", "js/behavior/app.contributor.behavior.js", "js/behavior/app.estlibishment.behavior.js", "js/behavior/app.passwordreset.behavior.js", "js/behavior/app.login.behavior.js", "js/behavior/app.faq.behavior.js",  "js/behavior/app.register.behavior.js", "js/behavior/app.settings.behavior.js", "https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&callback=initialize&key=" + sVal('google_maps_id') + "&sensor=true&libraries=geometry"];

		$.each(scripts, function(i, script) {
			$.loadScript(script);
		});

		window.initialize = function() {
			window.App.geocoder = new google.maps.Geocoder();
			window.App.notifyContributorInRange();
		};
	},
	autoLogin : function() {
		var app = window.App;
		$.ajaxProxy({
			url : "users/init",
			callback : function(response) {
				app.systemVariables = response.systemVariables;
				app.buildConfiguration();
				function process(d) {
					setTimeout(function() {
						if (window.App.currentPosition == null) {
							navigator.geolocation.getCurrentPosition(function(currentPosition) {
								window.App.currentPosition = currentPosition;
							}, function(e) {
							}, {
								timeout : 20000,
								enableHighAccuracy : true,
								maximumAge : 75000
							});
						}
					}, 30000);
					window.App.transitionType = d.user.User.transition_type;
					app.systemVariables = d.systemVariables;
					app.user = d.user.User;
					app.loadView(app.loadViewId, app.loadData);
				}

				if (response.user != null) {
					process(response);
				} else {
					var loginDetails = $.loadLoginDetails();
					if (loginDetails != null) {
						$.login(loginDetails, function(d) {
							process(d);
						});
					} else {
						app.loadView('login');
					}
				}
			}
		});
	}
};

