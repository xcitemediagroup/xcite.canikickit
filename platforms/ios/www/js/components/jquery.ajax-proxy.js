(function($) {
	$.extend({
		ajaxProxy : function(options) {
			options.timeout = window.RequestTimeout;
			options.cache = true;
			options.crossDomain = true;
			options.url = BASE_FULL_URL + options.url;
			options.type = options.type ? options.type : 'get';

			

			Pace.track(function() {
				$.ajax(options).done(function(data) {
					if (options.callback) {
						options.callback(data);
					}
				});
			});

		}
	});
})(jQuery);
