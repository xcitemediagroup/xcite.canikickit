(function($) {
	$.fn.extend({
		establishmentBrowser : function(d) {
			return this.each(function() {
				var $this = $(this);
				var html = '<div class="estiblisment-details-container" >';
				var length = d.all.length > 5 ? 6 : d.all.length;
				for (var i = 0; i < length; i++) {
					var est = d.all[i];
					html += '<div class="estiblisment-container" data-id="' + est.place_id + '" style="background-color:#333;">';
					html += '<table><tr>';
					html += '<td style="width:40px;"><div class="number" style="background-image: url(img/king-white.png);" ></div></td>';
					html += '<td ><div class="heading">' + est.name + '<br/>';
					html += '<div>' + est.vicinity + '</div></div></td></tr></table>';
					html += '</div>';
				}
				html += '</div>';
				$this.html(html);
				$this.find('.estiblisment-container').click(function() {
					var id = $(this).attr('data-id');
					window.App.loadView('estlibishment', id);
				});
			});
		}
	});
})(jQuery);

/*[{
 "geometry" : {
 "location" : {
 "lat" : 40.000883,
 "lng" : -105.021559
 }
 },
 "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png",
 "id" : "6e516a9651ae0e7fe9308cba607ac9760ee1683a",
 "name" : "The Lazy Dog Sports Bar and Grill",
 "opening_hours" : {
 "open_now" : true,
 "weekday_text" : []
 },
 "photos" : [{
 "height" : 960,
 "html_attributions" : [],
 "photo_reference" : "CnRnAAAA8Tmnn4fUm2V4o4i-1zfL4GlRKvCEJqGD5VwaO2vP8quWJyfeC-JwHnQaAt3XQSCexRLpBeZQ34_sZgYHV7HpG0SQ9_Wsl6zzON0fOf9SsevI-9KW33kKnsS33we-kJEwhznzYh8KmdHiRfm_eRleRxIQfVoNnFRLeGxcOFKuwy_YNRoUX67AP-z72r0X5ItQG3UtxDEf-5E",
 "width" : 720
 }],
 "place_id" : "ChIJO6uzo5AKbIcRLYxUaG0ueDo",
 "rating" : 3.8,
 "reference" : "CpQBggAAACWX3CY7WDTdUp2e2G7AgKpe5nEybpZqCnZtVAxqoKWXKj8hQH5ca3uKEIgyxfrZtN1kpsIs5rLlCGOJJbI1EyAlJaECT3E4c2VrhUWMZ_JGzkl3ibX_yXIEhQzrvociNNJhUzo6D8ZrU2GL12NVt2Y3oJ1r6ztZK7l3ChjglGbZ3UVx5qu6pTi7xy838PNHPRIQtBUiGUoMg5bJOxKcPozNhBoUKQKJ8iDBUASWnnfkPcMzmx7CSg0",
 "scope" : "GOOGLE",
 "types" : ["bar", "restaurant", "food", "establishment"],
 "vicinity" : "3100 Village Vista Drive, Erie"
 }, {
 "geometry" : {
 "location" : {
 "lat" : 40.000994,
 "lng" : -105.021097
 }
 },
 "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
 "id" : "ba219e03a90157874eb086f634079ded0cd790eb",
 "name" : "Si Senor! Real Mexican Food",
 "opening_hours" : {
 "open_now" : true,
 "weekday_text" : []
 },
 "place_id" : "ChIJFanim5AKbIcRyBOO2L1MYS4",
 "price_level" : 1,
 "rating" : 3.7,
 "reference" : "CoQBfAAAAIxDDiGzTOMx9crR_KU9-9L9mtSN-c1dYoias8U6XLL0M6-Z_6fRNYzRwpwVBwGPeuyuNL3qt9kMHtaofdC5mWOzMZ6NGf6GxKRGgi2Whw6ulxpARU4KI0SWSBsNbXblju_qlcIWh8Z33Eh3Fg9pOf9XpHqidfnO2mv-rQTxtiNEEhB5sqzM_1fWfCd6wrMUIsl6GhS_ARSgzLsO7K_JGisq9EBRmM_jJA",
 "scope" : "GOOGLE",
 "types" : ["restaurant", "food", "establishment"],
 "vicinity" : "3120 Village Vista Drive, Erie"
 }, {
 "geometry" : {
 "location" : {
 "lat" : 40.001173,
 "lng" : -105.020192
 }
 },
 "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/restaurant-71.png",
 "id" : "56a88a01e4a0aa297793e433f3005fe023242501",
 "name" : "Papa John's Pizza",
 "opening_hours" : {
 "open_now" : true,
 "weekday_text" : []
 },
 "photos" : [{
 "height" : 97,
 "html_attributions" : [],
 "photo_reference" : "CnRmAAAAIG-mhakYR3vxU9mVGI6wZiWgc7M-GuJxnFKTCWU9ScznJAdKNbuS9YxA13c4QGBIQ7jtLfNFpO7f2Hndp33MFydjLuYLqFmdm7uhe2y6YK_B-nra07fcq1Z6WYEU62j9oolM6zJJZhbOxj9kTyJAbBIQDHCDsupQL1KMdJF2Dv4tehoUf0Xrey-hncVxW9H6yMbGhgk0lw0",
 "width" : 145
 }],
 "place_id" : "ChIJ68VgpZAKbIcR_qoMtfshw-g",
 "price_level" : 1,
 "reference" : "CoQBcwAAAAdsn7f8d-S7IteqNKMbhJ-546JsZeaQQhY5J7D0h1YahL0y4u1XCre2yD0QaKDaQnuEh-VDgWtdPv6FPtnEzgki47Gx96owEUR456aNiFWH25pcM-luFlv6zqR_tc_Lr9IWJC0Ds4pYxDKiBqN81TCpY6EWu4q_OnTED-po9b3bEhCCOXmyEGhR-W7MfZBvpQ5IGhSI5yEu4RVUGA6KfGbv9MNa6T4phw",
 "scope" : "GOOGLE",
 "types" : ["meal_takeaway", "restaurant", "meal_delivery", "food", "establishment"],
 "vicinity" : "3140 Village Vista Ste 102, Erie"
 }, {
 "geometry" : {
 "location" : {
 "lat" : 40.005443,
 "lng" : -105.025836
 }
 },
 "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png",
 "id" : "5a6d51d8128e382ca19f06d4ffecfb75bae48462",
 "name" : "Master's Restaurant",
 "opening_hours" : {
 "open_now" : true,
 "weekday_text" : []
 },
 "place_id" : "ChIJMzu5uoQKbIcR_1MkbLvBsBM",
 "reference" : "CoQBdAAAALVEWDgWR5QACvUCvnho_iVgALqb1g9SSa0aHcRegrD_l0pyoezqX5MTzGyxhelUQfebf0YncOdzAmKm5U2TWQMlwq6-IVzWOgjVO1mFyVnpBriIDA9Bl2YflL6atwOigmf8gdb65LT4qMmZNK6XKkD5-j4LKHnNJDUrmwMZIRa-EhAy-_DTR1Ck0-VM-hIDQqB1GhTqb_UljvYra8SuSfdhvX3HJVT4Hg",
 "scope" : "GOOGLE",
 "types" : ["bar", "restaurant", "food", "establishment"],
 "vicinity" : "2700 Vista Parkway, Erie"
 }, {
 "geometry" : {
 "location" : {
 "lat" : 40.001275,
 "lng" : -105.01927
 }
 },
 "icon" : "http://maps.gstatic.com/mapfiles/place_api/icons/bar-71.png",
 "id" : "709dd8253e02049043fbcfdfabbec8193c8423a4",
 "name" : "Village Vista Wine & Spirits",
 "place_id" : "ChIJx3tuuJAKbIcRRQ4GR7G5cPI",
 "reference" : "CoQBfgAAAKcaf9lSzAiocouIVIRiEFxSr25jxI56Cld-B-gmgltHFfEbFbXNrBEVv4TLg-aOTEZJ1x7cz34y-0CitSB1iwmqIDM6wgsF7MDzfG0S_dNszB55Bpc_XRuMcq8LezrlmftcTvTfVxOjNLJiyiakueJlCGEmRBzxf4ejsQJsFFJHEhC05Q94ekfXbhwdYlvMkFBxGhQcWmg1InVJZFUseLcXirGRG8dURQ",
 "scope" : "GOOGLE",
 "types" : ["bar", "establishment"],
 "vicinity" : "3160 Village Vista Drive, Erie"
 }]*/