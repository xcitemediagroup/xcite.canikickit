(function($) {
	$.fn.extend({
		profileBrowser : function(promotion) {
			return this.each(function(){
				var $this = $(this);
				var app = window.App;
				var user = app.user;
				var html = '<div class="profile-browser-container"><div class="profile-image" ></div></div><br/>';
				$this.html(html).find(".profile-browser-container .profile-image").css('background-image', 'url(' + promotion.promotion.Promotion.featured_image + ')');
			});
		}
	});
})(jQuery);