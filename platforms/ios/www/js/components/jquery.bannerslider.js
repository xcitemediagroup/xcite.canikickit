(function($) {
	$.fn.extend({
		bannerSlider : function() {
			return this.each(function() {
				var $this = $(this);
				$.ajaxProxy({
					url : "advertisments.json",
					type : "get",
					callback : function(d) {
						var html = '<div class="banner-ads" data-fx="scroll" data-timeout="5000">';
						$.each(d.rows, function(i, item) {
							var p = item;
							html += '<img src="' + p.featured_image + '" data-url="' + p.url + '" />';
						});
						html += '</div>';
						$this.html(html);
						$('.banner-ads').tcycle();
						$this.find('.banner-ads img').click(function() {
							window.open($(this).attr('data-url'), "", "");
						});
					}
				});
			});
		}
	});
})(jQuery); 