$(function() {
	window.App.view.about.behavior = function() {
		var about = $.systetmVarByKey('about');
		var obj = about['SystemVariable'];
		
		$('#about [data-role="content"]').html('<h1>' + obj['name'] + '</h1><p>' + obj['value'] + '</p>');

		var copywrite = $.systetmVarByKey('copywrite');
		$('#about [data-role="content"]').append("<br/><div style='text-align:center;'>" + copywrite['SystemVariable']['value'] + "</div>");

		return {};
	};
});
