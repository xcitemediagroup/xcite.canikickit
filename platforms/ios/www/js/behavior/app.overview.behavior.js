$(function() {
	window.App.view.overview.behavior = function() {
		$('[data-role="contributor-map"], [data-role="establishment-browser"]').html("");
		$('#dashboard_name_c').html('loading...');
		var defaultPosition = null;
		var loadedData = null;
		$('.page-header, .search_container').hide();
		var lat = window.App.currentPositionSource.coords.latitude;
		var lng = window.App.currentPositionSource.coords.longitude;
		var location = lat + "," + lng;

		$.ajaxProxy({
			url : "contributors/allOfThem",
			type : 'POST',
			data : {
				location : location,
				keyword : ""
			},
			callback : function(d) {

				$('#overview [data-role="contributor-browser"]').contributorBrowser(d);
				$('#overview [data-role="establishment-browser"]').establishmentBrowser(d);


				$('.mapBtn').click(function() {
						alert("<div class='map-overlay' ></div>");
						var map = new google.maps.Map($('.map-overlay')[0], {
							zoom : 11,
							mapTypeId : google.maps.MapTypeId.ROADMAP,
							center : new google.maps.LatLng(39.7391667, -104.9841667),
							//styles : mapStyle,
							panControl : false,
							zoomControl : false,
							mapTypeControl : false,
							scaleControl : false,
							streetViewControl : false,
							overviewMapControl : false
						});
						$('.map-overlay').contibutorMap(d, map);
						$('.map-overlay').css({
							width : '100%',
							height : $(window).height() - $('.close').height()
						});
						$('.contributor-contianer-page').css('height', $(window).height() - $('.contributor-contianer').height() - 100);
				});
				loadedData = d;
				$('.page-header, .search_container').show();
				$("[data-role='search']").keyup(function() {
					var val = $(this).val();
					if (val != "") {
						$('.estiblisment-container').hide();
					} else {
						$('.estiblisment-container').show();
					}
					$('.estiblisment-container').each(function() {
						if ($(this).text().indexOf(val) > -1) {
							$(this).show();
						} else {
							$(this).hide();
						}
					});
				});
				$('#overview .wrapper').fadeIn();
				$('.contributor-contianer-page').css('height', $(window).height() - $('.contributor-contianer').height() - 100);

			}
		});
		return {};
	};
});
