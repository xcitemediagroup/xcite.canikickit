$(function(){
	
	window.App.view.register.behavior = function() {
		var app = window.App;
		var $submitBtn = $('#register .btnSubmit');
		var terms = $.systetmVarByKey('terms_conditions');
		
		$('.menu-button').hide();
		
		$('#terms-conditions-container').html('<label>' + window.App.view.register.labels.termsHeading + '</label><p>' + terms['SystemVariable']['value'] + '</p>');
		$('.login-toggle-btn').click(function(){
			$('#login-form').attr('data-social-type', 'kickit');
			$('#login-form').slideToggle(function() {
				$('#login-form input')[0].focus();
				
			});
		});
		
		var $loginSubmitBtn = $('#register .loginBtnSubmit');
		$loginSubmitBtn.click(function() {
			$.login({
				socialType : 'kickit',
				username : $('#login-form  [name="username"]').val(),
				password : $('#login-form  [name="password"]').val()
			}, function() {
				window.App.loadView('profile');
			});
		});
		$('#login-form').enterBind(function() {
			$loginSubmitBtn.trigger('click');
		});
		
			
		$submitBtn.click(function(){
			$('#register-form').ajaxFormSubmit(function(data) {
				var obj = {};
				$.each(data, function(i, item) {
					obj[item.name] = item.value;
				});
				$.register(obj, function(res){
					if(res.success){
						$.login(obj, function(){
							app.loadView('profile', 'first');
						});
					}else{
						alert(res.message);
					}
				});
			});
		});
		
		$('#register').enterBind(function(){
			$submitBtn.trigger('click');
		});	
		
		$("#terms").click(function(){
			$submitBtn[$(this).is(":checked") ? "fadeIn" : "fadeOut"]();
		});
		
		$('#register').attr('data-loaded', true);
		
		$('#register .featured_image_mask').click(function(){
			$('#register .featured_image').trigger("click");
		});
		$('#register .featured_image').parent().hide();
		
		
		return {};	
	};
});