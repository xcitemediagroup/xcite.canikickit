## README ##
Pure phone gap html5 app. 

Must be complied with adobe build suite to have functioning plugins. 

App will run in a browser with out compiling. 

Find provisioning files in the build directory.

##Directions to run Locally:##
You must have phone gap (cordova) installed as well as the phone gab dev tool app on your mobile phone. Navigate to your WWW directory. Then execute "phonegap serve" in your cmd line. Then on your mobile device launch the phonegap tool. Enter in the ip address from when you served phonegap.

##Directions to do a full build.##
Navigate to you www directory. Zip this directory. Then go to http://uild.phonegap.com. Create an account if you do not have one. Then login and make an app if you do not already have one. Click update source and upload the www file. Next it will prompt you for a security files for IOS. These are all generated from apple. once uploaded then build the apps. Right now I am set up for IOS and android. Once complete download the builds for IOS and Android. Next you will go to http://www.diawi.com/. Here you will upload the IOS build and an icon and it will generate a link to be able to install the app on any phone that have permissions to d so. Android will just let us put the app right on the device.

##Running in a browser.##
This app is pure HTML5. It will run in a browser as well. Just drop the files anywhere and run. The app brings in its native components through the config.xml ( which this way will only use approved adobe plugins and the source code stays fairly light.) When running in a browser the alerts will not use the native promoting and several features will not function including bar-code reader. I have put checks in for this so you can still run through all features in a browser but its not designed for this. Browser view is just for quick testing.